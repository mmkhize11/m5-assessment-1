import React, { useState, useEffect, Component } from 'react';
import { Text, View, StyleSheet, Button, FlatList,ListView  } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { AppBar } from '@react-native-material/core';
import {  HStack } from 'native-base';
// import { FontAwesome } from 'react-native-vector-icons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/FontAwesome';
// import { IconButton } from "@react-native-material/core";
import { IconButton } from 'react-native-paper';
// import firebase from "firebase";
import "firebase/storage";
import "firebase/auth";
import "firebase/firestore";

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

import firebase from '../config';
import { DocumentData } from '@google-cloud/firestore';
import { State } from 'react-native-paper/lib/typescript/components/TextInput/types';
import {
  faEye,
  faSearch,
  faBars,
  faSmile,faArrowLeft,
  faTrash
} from "@fortawesome/free-solid-svg-icons";
import { Appbar, FAB, useTheme } from 'react-native-paper';
import { color } from 'native-base/lib/typescript/theme/styled-system';
// import firestoreDocRef from '../config';
// import * as firebase from 'firebase/compat/app';
// import { initializeApp } from 'firebase/app';
// import { getFirestore, setDoc, doc } from 'firebase/firestore';
// import firestore from '@react-native-firebase/firestore';
export interface Props{
    navigation:any;
    user:any;
    
}

export interface AbcState {
  list: any[];
}


export default class DiscountList extends React.Component<Props,AbcState>{
  constructor(props){
  super(props);
  this.state ={ 
  list:[],
  } }

// export default function DiscountList({navigation}) {



  componentDidMount(){
    firebase.auth().onAuthStateChanged(async user => {
     firebase.firestore().collection('users').doc(user.uid).collection('Discounts').get().then((value)=>{
      var li = []
      value.forEach((child)=>{
       li.push({
        id:child.data()['id'],
        Barcode: child.data()['Barcode'],
        DiscountValue:child.data()['DiscountValue'],
        ID: child.data()['ID'],
      })
    })
   this.setState({list:li})
  })

})
 }


    // const FlatListBasics = () => {


    //     const [discountsList, setdiscountsList] = useState([])

    //     useEffect(() => {
    //         const loadData = async () => {
    //             firebase.auth().onAuthStateChanged(async user => {
    //                 if (user) {
    //                 //   const snapshot = await getDoc(doc(db, "users", user.uid))

    //                   const entityRef = await firebase.firestore().collection('users').doc(user.uid).collection('Discounts').get().then((document) => {
    //                     // const userData = document.data();
    //                         //   const querySnapshot =  await entityRef.get();
    //                 //   console.log(`What is current logged in user `+userData)
    //                 if (document.docs.length> 0) {
    //                   setdiscountsList(document.docs.map(doc => doc.data()))
    //                   document.docs.map((doc,index) => {
    //                     console.log(`document ${index}`, doc.data());
    //                     return doc.data();
    //                   });
    //                 }

    //                 });
                
    //                 }
    //               });

                  
               
    //             // const querySnapshot = await getDocs(collection(db, "Location"));

              
                
    //         }
    
    //         loadData()
    //     }, [setdiscountsList]);


       
    //     return (
    //       <View style={styles.container}>
    //         <FlatList
    //          data={discountsList}
    //           renderItem={({item}) => <Text style={styles.item}>{item.Barcode}</Text>}
    //         />
    //       </View>
    //     );
    //   }

      render(){
    return (
        <View style={styles.container}>
          <Appbar.Header
                style={{backgroundColor:'#f93b50'}}
      >

<Appbar.Action icon="arrow-left" onPress={() =>  this.props.navigation.replace('DashboardScreen')} />
<Appbar.Content title="Discount List " onPress={() => {}} />

      {/* <Appbar.Action icon="faSearch" onPress={() =>  this.props.navigation.replace('Scanner')} /> */}
      <IconButton
        icon="magnify"
        color={ '#ffffff'}
        // color={Colors.red500}
        size={20}
        onPress={() =>  this.props.navigation.replace('Scanner')} 
      />
  
    

      </Appbar.Header>

            <FlatList
              data={this.state.list}
              keyExtractor={(item)=>item.ID}
              renderItem={({item}) =>
              //  <Text style={styles.item}>{item.Barcode}</Text>
               <View style={styles.row}>
              {/* <Text style={styles.rowText}>{item.Barcode}</Text> */}

              <Text style={styles.rowText}>{"R"+item.DiscountValue+" off any order as your first purchase"}</Text>
            
            
              <IconButton
        icon="delete"
        color={ '#FF0000'}
        // color={Colors.red500}
        size={20}
        onPress={() => {

      
          firebase.auth().onAuthStateChanged(async user => {

            if(user==null){
               user = firebase.auth().currentUser;
            }

            // console.log(`what is user now   ${JSON.stringify(user)}`)

            console.log(`what is item being deleted  ${JSON.stringify(item)}`)
            firebase.firestore().collection('users').doc(user.uid).collection('Discounts').doc(item.id).delete().then((response)=>{
            
            
            console.log(`what is response   ${JSON.stringify(response)}`)
              firebase.firestore().collection('users').doc(user.uid).collection('Discounts').get().then((value)=>{
                var li = []
                value.forEach((child)=>{
                 li.push({
                  Barcode: child.data()['Barcode'],
                  DiscountValue:child.data()['DiscountValue'],
                  ID: child.data()['ID'],
                })
              })
              const found = li.findIndex(country => country.Barcode === item.Barcode);

              console.log(`what is index found    ${found}`)


              if(found>-1){
li.splice(found,1)
              }
              // li.findIndex()
             this.setState({list:li})
            })
            });


           
       })

       
        }  } 
      />
              {/* <button onClick={() =>  alert('time to delete')} > <FontAwesomeIcon icon={faTrash}  /></button>
              
              */}
              {/* <Icon name="ios-eye" type="ionicon" color="#C2185B" /> */}
            </View>
              
              }
            
            />

{/* <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) => (
            <View style={styles.row}>
              <Text style={styles.rowText}>{rowData}</Text>
              <Icon name="ios-eye" type="ionicon" color="#C2185B" />
            </View>
          )}
        /> */}
       
       {/* </View> */}
        </View>
        
      );
}

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
    },
    row: {
      margin: 15,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: 2,
    },
    rowText: {
      fontSize: 18,
    },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  });