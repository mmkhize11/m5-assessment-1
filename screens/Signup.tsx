import React,{useState} from 'react'
import { StyleSheet, Text, ScrollView,View,StatusBar,Image,TextInput, TouchableOpacity } from 'react-native'
import {Colorstobeused} from '../constants/color'
import Icon from 'react-native-vector-icons/FontAwesome'
import Buttons from '../components/buttons'

import '@firebase/auth';
import '@firebase/firestore';
import firebase from '../config';
import { IconButton } from 'react-native-paper';

const Signup = ({navigation}) => {

    // const [formData,setformData] = useState({
    //     firstName:'',
    //     lastName:'',
    //     email:'',
    //     phone:'',
    //     password:''
    // })



  
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')


    const onRegisterPress = () => {
        if (password !== confirmPassword) {
            alert("Passwords don't match.")
            return
        }
    
        firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then((response) => {
                const uid = response.user.uid
                const data = {
                    id: uid,
                    email,
                    firstName,
                    lastName,
                    phone
                };
                const usersRef = firebase.firestore().collection('users')
                usersRef
                    .doc(uid)
                    .set(data)
                    .then(() => {
                        navigation.navigate('DashboardScreen', {user: data})
                    })
                    .catch((error) => {
                        alert(error)
                    });
            })
            .catch((error) => {
                alert(error)
        });
    }

    return (
        <ScrollView style={{flex:1,backgroundColor:'#fff',flexDirection:'column'}}>
            <StatusBar barStyle="dark-content" backgroundColor="#fff" />
            {/* login form section */}
            <View style={{flex:2,flexDirection:'column',backgroundColor:'#fff',paddingTop:10,paddingHorizontal:'3%'}} >
                <View style={{flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}} >
                    <Text style={{fontFamily:'OpenSans-Bold',fontSize:30,color:Colorstobeused.black}} >Join the Family </Text>
              
                </View>





                <View style={{flexDirection:'column',paddingTop:20}} >

                <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#ededed',width:'95%',borderRadius:10,height:60,paddingLeft:20,marginBottom:20}} >
                        <Icon name="user" size={22} color="#818181" />
                        <TextInput onChangeText={(text)=>{setFirstName(text) }} style={styles.input} placeholder="Enter First Name" placeholderTextColor="#818181" />

                    </View>

                    <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#ededed',width:'95%',borderRadius:10,height:60,paddingLeft:20,marginBottom:20}} >
                        <Icon name="user" size={22} color="#818181" />
                        <TextInput onChangeText={(text)=>{setLastName(text)}} style={styles.input} placeholder="Enter Last Name" placeholderTextColor="#818181" />

                    </View>


                    <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#ededed',width:'95%',borderRadius:10,height:60,paddingLeft:20,marginBottom:20}} >
                        <Icon name="envelope-o" size={22} color="#818181" />
                        <TextInput onChangeText={(text)=>{setEmail(text)}} style={styles.input} placeholder="Enter Email" placeholderTextColor="#818181" />

                    </View>

                    {/* phone */}
                    <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#ededed',width:'95%',borderRadius:10,height:60,paddingLeft:20,marginBottom:20}} >
                        <Icon name="phone" size={22} color="#818181" />
                        <TextInput onChangeText={(text)=>{setPhone(text)}} style={styles.input} placeholder="Enter Phone" placeholderTextColor="#818181" />

                    </View>

                    <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#ededed',width:'95%',borderRadius:10,height:60,paddingLeft:20,marginTop:20,marginBottom:20}} >
                        <Icon name="lock" size={22} color="#818181" />
                        <TextInput onChangeText={(text)=>{setPassword(text)}} style={styles.input} placeholder="Enter Password" secureTextEntry={true} placeholderTextColor="#818181" />
                    </View>

                    <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center',backgroundColor:'#ededed',width:'95%',borderRadius:10,height:60,paddingLeft:20,marginTop:20,marginBottom:20}} >
                        <Icon name="lock" size={22} color="#818181" />
                        <TextInput onChangeText={(text)=>{setConfirmPassword(text)}} style={styles.input} placeholder="Confirm Password" secureTextEntry={true} placeholderTextColor="#818181" />
                    </View>


                    <Buttons  btn_text={"Register"} on_press={()=>
                        onRegisterPress()
                        // console.log(formData)
                        } />


<View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'flex-end',backgroundColor:'#fff',marginBottom:40}} >
                    <Text style={{fontFamily:'OpenSans-Medium',fontSize:17,color:'#000000'}} >Have an account ? </Text>
                    {/* <Text style={{fontSize:18,fontFamily:'OpenSans-SemiBold',color:'#f93b50'}}  >Sign Up</Text> */}
                    <Text style={{fontSize:18,fontFamily:'OpenSans-SemiBold',color:'#f93b50'}} onPress={ ()=> navigation.navigate('Login') } >Sign In</Text>
       
                </View>
            </View>
            </View>

         
            
        </ScrollView>
    )
}

export default Signup

const styles = StyleSheet.create({
    input:{
        position:'relative',
        height:'100%',
        width:'90%',
        fontFamily:'OpenSans-Regular',
        paddingLeft:20,
    },
    social_btn:{
        height:55,
        width:'100%',
        borderWidth:1,
        borderRadius:10,
        borderColor:'#ddd',
        flexDirection:'row',
        alignItems:'center',
        marginBottom:20
    },
    social_img:{
        width:25,
        height:25,
        marginLeft:15
    }
})