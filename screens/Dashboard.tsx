import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Dashboard from 'react-native-dashboard';
// import { FontAwesome } from 'react-native-vector-icons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { AppBar } from "@react-native-material/core";
import { HStack } from 'native-base';
import { IconButton } from 'react-native-paper';
const Icon = ({ icon, item, background }) => (
    <FontAwesome
        name={icon}
        size={40}
        color={
            item.iconColor || (!item.background || !background ? '#3498db' : '#fff')
        }
        style={item.styleIcon}
    />
);

const data = [
    {
        name: 'New Stock',
        background: '#f93b50',
        icon: (item, background) => Icon({ icon: 'gratipay', item, background }),
        styleIcon: { color: '#ffffff' },
        onclick:()=>{
            // navigation.replace('DashboardScreen')
        }
        //     navigation.replace('DashboardScreen')
    },
    {
        name: 'Profile',
        background: '#f93b50',
        icon: (item, background) => Icon({ icon: 'user', item, background }),
        iconColor: '#ffffff',
        rippleColor: '#000',
     
    },
    {
        name: 'Purchases',
        background: '#f93b50',
        icon: (item, background) => Icon({ icon: 'gratipay', item, background }),
        styleIcon: { color: '#ffffff' },
    },
    {
        name: 'Discounts',
        background: '#f93b50',
        icon: (item, background) => Icon({ icon: 'heart', item, background }),
    },
    {
        name: 'Specials',
        background: '#f93b50',
        icon: (item, background) => Icon({ icon: 'users', item, background }),
        styleName: { color: '#ffffff', fontWeight: 'bold' },
    }
];

const DashboardScreen =  ({navigation}) => {
    const card = ({ name }) => {
        // console.log('Card: ' + name)

        switch(name){
            case 'New Stock':{
                navigation.replace('Home')
                break;

            }

            case 'Discounts':{
                // DiscountList
                navigation.replace('DiscountList')

                break;
                
            }

            case '':{
                break;
                
            }

            default:{

            }
        }
    };
    return (
        <View style={styles.container}>
            <AppBar
            style={{backgroundColor:'#f93b50'}}
    title="Dashboard"
    leading={props => (
        <IconButton
        icon="menu"
        color={ '#ffffff'}
        // color={Colors.red500}
        size={20}
        onPress={() => console.log('Pressed')}
      />
    //   <IconButton icon={props => <Icon name="menu" {...props} />} {...props} />
    )}
    trailing={props => (
      <HStack>
         <IconButton
        icon="magnify"
        color={ '#ffffff'}
        // color={Colors.red500}
        size={20}
        onPress={() => console.log('Pressed')}
      />
        <IconButton
        icon="dots-vertical"
        color={ '#ffffff'}
        // color={Colors.red500}
        size={20}
        onPress={() => console.log('Pressed')}
      />
      </HStack>
    )}
  />
            <Dashboard
                data={data}
                background={true}
                card={card}
                column={2}
                rippleColor={'#3498db'}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ecf0f1',
    },
});


export default DashboardScreen;